# Tools Description
## Connect to VM
```
./ssh [hostname]
```
- The list of [hostname] can be seen in config file.
- Please run `git pull origin HEAD` if the command doens't work, because probably the IP address has been updated.

## Upload File to VM
```
./scp [hostname] [source] [target]
```
- [target] is path relative to **/home/[user_in_vm]** in vm.
Blank [target] will cause the file to be sent to **/home/[user_in_vm]** with the same name in local.
- [source] is path of file to be uploaded relative to current directory.

Example of sending the config to nginx server:
```
./scp nginx config config
```

## Download File from VM
```
./scpr [hostname] [target] [source]
```
- [source] is file or directory relative to **/home/[user_in_vm]** which is intended to be downloaded to local.
- [target] is path relative to current directory in local.

Example of downloading the config file from nginx server:
```
./scpr nginx config config
```
